#!/bin/bash

echo "..."
echo "G988U Android R Kernel Build Script"
echo "..."
echo "by"
echo "..."
echo "elliwigy"
echo "..."
echo "Setting ARCH and making out directory..."
echo "..."
export ARCH=arm64
mkdir out
echo "Please edit the ccache script in build_kernel.sh if you do not have ccache setup or if you do not want to use it or to adjust the amount of space allocated for ccache. This will setup to use ccache including ccache directory as well as the ccache binary and sets ccache to use 50gb..."
export USE_CCACHE=1
export CCACHE_DIR=~/.cache
export CCACHE_EXEC=/usr/bin/ccache
ccache -M 50G
ccache -s
echo "..."
echo "Setting up build environment variables to use with the make commands..."
BUILD_CROSS_COMPILE=$(pwd)/toolchain/gcc/linux-x86/aarch64/aarch64-linux-android-4.9/bin/aarch64-linux-android-
KERNEL_LLVM_BIN=$(pwd)/toolchain/llvm-arm-toolchain-ship/10.0/bin/clang
CLANG_TRIPLE=aarch64-linux-gnu-
KERNEL_MAKE_ENV="DTC_EXT=$(pwd)/tools/dtc CONFIG_BUILD_ARM64_DT_OVERLAY=y"
echo "..."
echo "First we will set the config then run the make command to build the kernel..."
echo "..."
make -j8 -C $(pwd) O=$(pwd)/out $KERNEL_MAKE_ENV ARCH=arm64 CROSS_COMPILE=$BUILD_CROSS_COMPILE REAL_CC=$KERNEL_LLVM_BIN CLANG_TRIPLE=$CLANG_TRIPLE vendor/z3q_usa_singlew_defconfig
make -j8 -C $(pwd) O=$(pwd)/out $KERNEL_MAKE_ENV ARCH=arm64 CROSS_COMPILE=$BUILD_CROSS_COMPILE REAL_CC=$KERNEL_LLVM_BIN CLANG_TRIPLE=$CLANG_TRIPLE
echo "Now we will copy the Image/kernel to arch/arm64/boot/Image..."
cp out/arch/arm64/boot/Image $(pwd)/arch/arm64/boot/Image
echo "..."
echo "Now we are all finished! If it didn't build correctly read the errors and solve them..."
echo "ENJOY!"
